% ------------------------------------------------------------------------ %
% 	INTRODUCTION
% ------------------------------------------------------------------------ 
%
\cleardoublepage
%
\chapter{Introduction}
%
\label{cap:introduction}
%
% ------------------------------------------------------------------------ %
The protection of aircraft from the adverse effects of ice accretion has been a crucial design problem since the very early years of flight. During its lifespan, an aircraft experiences a wide variety of environmental
conditions, which often change abruptly during normal operation and manufacturers must guarantee safety against all possible atmospheric hazards such as severe rain, snow and ice \cite{Gent2000} \cite{Gori2018}.
If an aircraft flies through visible water such as rain or cloud droplets and if the temperature at the point where they impact is below the freezing temperature, the water particles can freeze creating an ice shape on the most exposed zones such as wing leading edges, engine intakes, Pitot tubes, propellers and vents. \\
Ice formations have significant and crucial effects on performances as they modify the aerodynamic shape of wings as well as rotorcraft blades altering their ability to create lift. Furthermore, the increasing drag caused by ice needs to be compensated by an additional power, the nose is lifted up to maintain altitude, the angle of attack increases and this enhances the wetted surface allowing the aircraft to accumulate additional ice. Wind tunnel tests have shown that snow or ice accumulations no thicker than a piece of coarse sandpaper can reduce lift by 30 \% and increase drag by 40 \% \cite{AircraftIcing}. Larger accretions can increase drag by 80 \% or even more. In extreme cirumstances the aircraft can be so iced that it can stall at much lower angle of attacks than normal and it can roll or pitch uncontrollably as ice may affect the aircraft stability changing the weight distribution. Ice can also cause engine failure either by blocking or damaging compressor blades or icing the carburator.

\begin{figure}

	\begin{minipage}{0.5\linewidth}
		\centering
		\includegraphics[width=.7\textwidth]{Images/Introduction/Engine_iced.jpg}
		\vspace{4ex}
	\end{minipage}
	\begin{minipage}{0.5\linewidth}
		\centering
		\includegraphics[width=.7\textwidth]{Images/Introduction/wing_iced.jpg}
		\vspace{4ex}
	\end{minipage}
	\begin{minipage}{0.5\linewidth}
		\centering
		\includegraphics[width=.7\textwidth]{Images/Introduction/Pitot_iced.png}
		\vspace{4ex}
	\end{minipage}	
	\begin{minipage}{0.5\linewidth}
		\centering
		\includegraphics[width=.7\textwidth]{Images/Introduction/Helicopter_iced.png}
		\vspace{4ex}
	\end{minipage}
	
\caption{Example of ice accretion on an engine, wing, Pitot probe and helicopter. Images taken from \cite{Potapczuk2013}, \cite{IcedWing}, \cite{IcedPitot} and \cite{IcedHelicopter}}
\label{fig:IceExamples}

\end{figure}

\noindent
As shown in figure \ref{fig:IceAccidents}, on a total number of 3230 accidents caused by adverse weather conditions during 1990s, 12 \% were caused by ice and 27 \% involved fatalities. For these reasons, since 1940s and 1950s, signi­ficant experimental and flight test programmes have been performed to investigate the physics of ice accretion in order to develop systems to protect the aircraft from ice damages. The early works by Hardy \cite{Hardy1947} and Messinger \cite{messinger1953equilibrium} represent a milestone in numerical analysis of aircraft icing. Messinger developed the first, and still in use, model of ice accretion over a surface. With the advent of the computer in the 1970s, significant progress on theoretical studies and simulations have been achieved. \\
In the following years many research centers such as NASA \textit{Lewis Research Center} in the United States, the \textit{Defence Evaluation and Research Agency} (DERA) in the United Kingdom, the \textit{Office National d’Etudes et de Recherches Aérospatiales} (ONERA) in France and \textit{Centro Italiano Ricerche Aerospaziali} (CIRA) in Italy contributed to the progress on the study of ice physics thanks to many experimental campaigns and the development of codes which can predict the ice formation. Some codes that have been developed during the years are  LEWICE \cite{Ruff1990, Wright2008}, CANICE \cite{Brahimi1994}, TRAJICE \cite{gent1990}, FENSAP-ICE \cite{Morency2001}, ICEREMO \cite{Press2000} and POLIMICE \cite{Gori2015}
They are nowadays a fundamental tool in aircraft design. As well as to predict ice shapes, they are used to investigate performances degradation of lifting surfaces and to aid the design of anti or de-icing systems. The simulation of in-flight ice accretion requires the computation of the flow around the aircraft. The flow is composed by air and water droplets, thus a multiphase simulation must be performed. Once aerodynamic datas and the impact rate of droplets on the surface are known, ice accretion is computed basically solving the mass and energy balance for each element of the surface mesh. The mathematical models governing ice accretion are explained in detail in chapter \ref{cap:icemodels}.

\begin{figure}

	\centering
	\begin{tikzpicture}
		\centering
		\pie [explode = 0.2, radius = 2]
			{88/ All Weather (2842), 12 /Icing (388)}
	\end{tikzpicture}

\caption{Accidents caused by ice respect to other weather conditions (1990-2000) \cite{AircraftIcing}}
\label{fig:IceAccidents}

\end{figure}
%
% ------------------------------------------------------------------------ 
%
\section{Fundamentals of ice accretion}
%
% ------------------------------------------------------------------------ %
The present section reports a brief explanation on how ice accretes and the fundamental parameters governing the physics of the problem. \\ 
Aircraft icing is de­fined as flight in cloud at temperatures at or below freezing, when supercooled water droplets impinge and freeze on the unprotected areas on which they impact \cite{Gent2000}. When a component of an aircraft flies through a supercooled droplet cloud, the impinging droplets will try to release their latent heat and freeze to form ice. Water droplets in the supercooled state are unstable because they have a temperature below the freezing point but they are still liquid. When they impact on a cold surface the equilibrium breaks and they quickly freeze. Due to their relatively small size, cloud droplets may frequently exist in the supercooled state down to \SI{-20}{\celsius} and, less frequently, down to \SI{-30}{\celsius} and \SI{-35}{\celsius}.

\subsection{Ice types}
Depending on the external weather conditions, three types of ice can form on an exposed surface: \textit{rime} ice, \textit{glaze} ice and \textit{mixed} ice.

\subsubsection{Rime ice}
\noindent
Rime ice forms when droplets are very small and the liquid portion freezes immediatly after the impact before having the time to spread over the aircraft surface. This very fast phenomenon traps some air bubbles inside the ice giving to it a white and opaque appereance. The density is usually quite low. Figure \ref{fig:rime} shows a rime formation over a wind tunnel model and its peculiar aspect is clearly seen. It tends to form at combinations of low ambient temperature, low speed and a low value of \textit{Liquid Water Concentration}. Rime ice is lighter than glaze ice but its rough surface leads to a significant degradation of aerodynamic performances. However, due to its brittleness, it can be easily removed by ice protection systems .

\subsubsection{Glaze ice}
\noindent
Glaze ice forms when, after the initial impact, the remaining liquid portion of the droplet flows over the surface forming a runback water film. The droplet can freeze aft respect to the impact point, producing localized thickening of the ice which may lead to the formations of horns, typical of the glaze type. In glaze conditions, the density of accreted ice is larger than rime ice. It forms at temperatures close to the freezing point, when water does not freeze istantaneously, high velocities or high \textit{Liquid Water Concentration}. As showed in figure \ref{fig:glaze}, its aspect is translucent due to the water film flowing over its surface and it is very hard, heavy and tenacious. Therefore, is very difficult to be removed by an ice protection system.

\TwoFig{Images/Introduction/rimeice.jpg} % image 1
         {Example of rime ice \cite{Rime}}  % caption 1
         {fig:rime} % label 1
         {Images/Introduction/glazeice.jpg} % image 2
         {Example of glaze ice \cite{Glaze}} % caption 2
         {fig:glaze} % label 2
         
\subsubsection{Mixed ice}
\noindent
Mixed ice is an intermediate condition between rime and glaze and it forms when droplets vary in size or when they are mixed together with snow.         
         
\subsection{Icing relevant parameters}

The rate and amount of ice accretion on a surface depends on the \textit{air temperature}, the \textit{shape}, the \textit{surface roughness}, the \textit{airspeed}, the \textit{liquid water concentration} (LWC), \textit{the size of the droplets} in the cloud and the \textit{collection efficiency} \cite{Gent2000}.

\subsubsection{Air and surface temperature} 
\noindent
The most critical parameter which governs ice accretion is the temperature as it controls the heat exchanges between the aircraft and the surrounding air: the lower it is, the faster ice accretion is as heat fluxes are larger. The surface temperature is also involed in the evaluation of heat fluxes and it is important as it controls the capability of the surface to accumulate ice and it dictates whether accretion is possible and the rate at which accretion proceed. With a thermal protection system ­the aim is to raise the temperature of the surface above the freezing point in order to prevent the formation of ice shapes.

\subsubsection{Surface roughness}
\noindent
The surface roughness has a lot of influence on the final ice shape. It is a very sensitive parameter in the computation of the convective heat transfer coefficient between the surface and air or water. Roughness influences the boundary layer transition point and thus, besides the heat transfer coefficient, plays an important role also in the determination of the skin friction coefficient. Furthermore, surface roughness affects the interation of the droplet with the surface at the impact point. In fact, when a droplet impacts it can rebound, splash or stick on the surface and this behaviour is influenced by roughness as well as other parameters.

\subsubsection{Airspeed}
\noindent
Airspeed influences the amount of droplets which are collected by the surface. High velocities let the surface collect more water and hence the possibility of ice accretion increases. 

\subsubsection{Liquid Water Concentration (LWC)}
\noindent
The \textit{Liquid Water Content} (LWC) represents the number of grams of water per cubic meter of air. The cloud LWC affects both types of ice and their accretion rate. For large values of LWC, the latent heat which has to be removed to freeze completely the droplets is also large and that leads to the formation of glaze ice. As air temperature decreases, the probability of encountering a large amount of droplets decreases, hence the combination low temperature and low LWC tends to lead to rime ice formation. In general, the mass of water at the surface of a body will increase linearly with the LWC. Hence, the greater the value of the LWC, the greater is the potential for a large accumulation of ice \cite{Gent2000}. The observed value of a cloud LWC depends on the cloud type and their typical values are reported in table \ref{tab:cloudLWC}.

\begin{table}

	\centering

	\begin{tabular}{llc}
	\toprule
	Environment & Cloud Type & LWC (\si{\gram\per\cubic\metre}) \\
	\midrule
	\multirow{6}{*}{Continental} & Stratus & 0.28 \\
	& Cumulus (clean) & 0.26 \\
	& Cumulus (polluted) & 0.3 \\
	& Cumuloninbus (growing) & 1-3 \\
	& Cumuloninbus (dissipating) & 1.0-1.5 \\
	& Fog & 0.06 \\
	\hline
	\multirow{2}{*}{Maritime} & Stratus & 0.30 \\
	& Stratocumulus & 0.44 \\
	\hline
	\multirow{2}{*}{Continental or Maritime} & Cirrus (\SI{-25}{\celsius})   	& 0.03 \\
	& Cirrus (\SI{-50}{\celsius}) & 0.002 \\
	\bottomrule
	\end{tabular}
	
	\caption{Typical observed cloud LWC \cite{CloudLWC}}
	\label{tab:cloudLWC}
	
\end{table}

\subsubsection{Mean Volume Diameter (MVD)}
\noindent
The \textit{Mean Volume Diameter} (MVD) is the representative value of the size of a droplet. Within a cloud, droplets have a wide range of sizes  but their diameter can be described with a probability density function. The MVD is the value above and below which half the volume of water is contained. Half the water volume will be in larger drops and half the volume in smaller drops \cite{MVDDef} \cite{Gent2000} . The size of droplets affects a lot the the impingement limits and the capability of the surface to collect water. Droplets with large diameter, and therefore large inertia, are less affected by aerodynamic forces and tend to follow a straight trajectory. On the other hand, droplets with small diameter tend to follow the air streamlines. The MVD is strongly dependent on atmospheric conditions, especially on temperature. As the ambient temperature decreases, droplets tend to minimize the heat exchange with the surrounding air and thus there is the tendency for the diameter to reduce. As a consequence of that, small droplets are typical in low temperature clouds while large droplets are more common in warmer clouds. Typical values of the MVD are between \SI{15}{\micro\metre} and \SI{40}{\micro\metre} but in recent years the icing hazard posed from cloud with larger droplets diameter has become very imporant. They are called as \textit{Supercooled Large Droplets} (SLD) and their MVD spaces between \SI{40}{\micro\metre} and \SI{400}{\micro\metre}. The condition in which SLD form is known as freezing rain and it is typically associated to temperatures between \SI{-15}{\celsius} and \SI{5}{\celsius} and relatively low LWC.
 
\subsubsection{Collection efficiency $(\beta)$}
\noindent
The \textit{collection efficiency}, usually indicated with $\beta$, is one of the most fundamental parameters in ice accretion. It represents a measure of the water quantity collected by the body and it is an index of how much the surface is wet. According to figure \ref{fig:Beta}, it is defined as the ratio between the farfield area and the surface area both enclosed by the same droplets trajectories:
\begin{equation}
\beta = \frac{dA_\infty}{dA_i}		
\end{equation}

\begin{figure}

	\centering
	\begin{minipage}{0.7\linewidth}
		\centering
		\includegraphics[width=.9\textwidth]{Images/Introduction/Beta_2D.png}
		\vspace{4ex}
	\end{minipage}
	\begin{minipage}{0.7\linewidth}
		\centering
		\includegraphics[width=.9\textwidth]{Images/Introduction/Beta_3D.png}
		\vspace{4ex}
	\end{minipage}
	
\caption{Definition of collection efficiency over a 2D and a 3D geometry \cite{Gent2000}}
\label{fig:Beta}

\end{figure}

\noindent
Typical values of $\beta$ space between 0 on a clean surface and 0.7-0.8 close to stagnation points, where the surface collects more water. As shown in chapter \ref{cap:icemodels}, the collection efficiency is directly proportional to the accretion rate and therefore, an high value of $\beta$, implies an high ice accretion rate. The collection efficiency is strongly influenced by several parameters like the airfoil chord, the airspeed and the MVD. Studies on the influence of these parameters on $\beta$ can be found in \cite{Gent2000}.
%
% ------------------------------------------------------------------------ 
%
\section{Ice Protection Systems}
%
% ------------------------------------------------------------------------ 
%
In order to guarantee safe flight in icing conditions, commercial and
some military aircraft are equipped with \textit{Ice Protection Systems} (IPS) to minimise the impact caused by ice accumulated on the most exposed zones. Ice protection systems are deployed into aircraft to delay or to remove any ice accumulation on its surface and its components and therefore to maintain the in-flight performance and safety. The two main categories of IPS are the \textit{de-icing} systems and the \textit{anti-icing} systems. The former cyclically operates to remove the ice layer formed after some exposition period. When the system is not actuated, the ice builds up on the surface; when it is actuated, the system removes the ice formation. On the other hand, the anti-ice system prevents any ice accretion on surfaces and continuously operates while the aircraft flies under icing conditions. Different technologies can be combined within the same aircraft for accomplishing the most convenient and efficient protection. In this section, the most used and developed technologies will be presented. 

\subsubsection{De-icing boot}
\noindent
This technology is very mature and proven as it has been developed in the early 1930s. It consists on a set of flexible rubber-like boots positioned on ice-prone areas which expand and contract cyclically according to the demanded protection, as shown in figure \ref{fig: Boots}.

\begin{figure}

	\centering
	\includegraphics[width=.5\textwidth]{Images/Introduction/Boots.png}

	\caption{Goodrich pneumatic boots \cite{AircraftIcing}}
	\label{fig: Boots}

\end{figure} 

\noindent
Boots are classified as de-icing systems as they remove an already formed layer of ice from surfaces where they are located. Following the crack, aerodynamic forces shed the ice which is carried downstream by the flow. Depending on the type of aircraft, boots are inflated with bleed air taken from engines or an additional pneumatic system is included for that purpose. To improve the performance of these systems, fluid freeze point depressant and chemical solutions that reduce the ice adhesion can be sprayed over the rubber boots.\\
One of the drawbacks of the technology is the degradation of the rubber boots and consequently of the performance caused by interaction with extreme environmental conditions, such as very low temperatures, UV radiation and atmospheric moisture. Therefore boots need frequent maintainance and repairs in order to ensure an adequate safety level. Moreover, studies observed that the cyclic process does not remove the entire ice formation \cite{Broeren2004}.

\subsubsection{Pneumatic-Thermal protection}
\noindent
This technology is widely used and it has been deployed in numerous commercial, defence and business aircraft. Pneumatic-thermal protections use hot and pressurized air bled from one or more engine compressor stages. The air is supplied to the air cycle machines and exhausted through the \textit{piccolo tube} of the IPS to provide thermal ice protection to surfaces at risk of ice accretion \cite{Chakraborty2015}. A schematic view of the system is shown in figure \ref{fig:Piccolotube}.

\begin{figure}

	\centering
	\includegraphics[width=.5\textwidth]{Images/Introduction/Piccolotube.png}

	\caption{Cross section of a piccolo tube \cite{Donatti2007}}
	\label{fig:Piccolotube}

\end{figure} 

\noindent
The pressurised air moves across the perforations which are directed to the areas intended to be protected heating the outer surface. The pneumatic-thermal protection can be very inefficient because the temperature of the air bled from the engine must be down-regulated for safety reasons before being used by the IPS \citep{Chakraborty2015}. Even though the leading edge is mitigated by the IPS, since the temperature of the surface can be below the freezing point, the water can flow downstream and freeze aft in unprotected areas. This particular situation is called runback water and it is absolutely relevant because it might generate ice formations which cannot be removed by an IPS. To mitigate the risk of runback water, thermal IPS are designed to work in fully or partially evaporative conditions which require a large energy consumption. This technology can be used both in de-icing or anti-icing conditions, depending if it is activated before or after the formation of ice. 

\subsubsection{Thermo-Electric protection}
\noindent
The thermo-electric protection is one of the most used nowadays and it is the preferred method for rotorcrafts because it presents the best fit to the design, due to the relatively small blades thickness. Furthermore, this protection is installed in many aircraft components which are susceptible to ice formation like Pitot probes. Likewise to the pneumatic protection, the thermo-electric heats surfaces at risk of ice accretion but the heat is generated by an electric current going through a resistive
component which can be an internal coil wire, a conductive film or an heating rod. The resistance can be bonded on the inner surface of the metallic leading edge or embedded directly into the matrix of a composite material \cite{DeRosa2010}. Also this technology can be used in de-icing or anti-icing mode, depending if it is activated before or after the formation of ice and can be also partially or fully evaporative, depdending on the temperature reached by the surface. 

\subsubsection{Chemical protection}
\noindent
This technology is mainly used on ground operations before the flight. Foams and chemical substances like glycol are sprayed on surfaces at risk of ice accretion in order to lower the freezing point of water or to break the adhesion force of ice which is then shed by aerodynamic forces. Chemical protections have been deployed also on aircrafts, like in the case of the Hawker 800XP \cite{Hawker800XP}. A micro-perforated panel is mounted into the leading edge and the fluid is pumped by means of an elctrical pump from an external tank through the holes. This technology is very efficient from the energy point of view but it has many drawbacks which explain why it is not widely used in aviation. The main disadvantage is represented by the dependency on the quantity of fluid stored in the tank and, furthermore, the installation of the tank leads to an additonal weight that must be carried.

\subsubsection{Other technologies}
\noindent
As well as mature IPS, in recent years many technologies are under development which can provide more reliable and convenient protection. The most important is the use of super-hydrophobic materials which can be applied over surfaces at risk of ice in order to reduce the adhesion force of the droplet \cite{ANTONINI201158}. This passive technology is very efficient as it does not need energy but it needs a continuous maintainence as the surface has to be always clean from dust and other impurities. Furthermore, ultrasound waves can be used to create stresses on ice causing its shedding \cite{Budinger2016}. Finally, nanomaterials 
such as conductive polymer nanocomposites can be used thanks to their tailorable and attractive heating properties. The IPS uses Joule heating
of aligned carbon nanotube (CNT) arrays to create highly efficient de-icing and anti-icing of surfaces \cite{Buschhorn2013}.
%
% ------------------------------------------------------------------------ 
%
\section{Thesis goal and outline}
%
% ------------------------------------------------------------------------ 
%
The work presented in this thesis focuses on the development of a new tool for ice accretion simulations which also includes a model of a 2D thermo-electric anti-ice system. This work fits in a project already under way at Politecnico di Milano which aims at the development of an highly modular framework for the simulation of in-flight ice accretion, named PoliMIce. Some limitations emerged from the available version of the PoliMIce code, in particular its structure did not allow an easy implentation of new features, like the modeling of an IPS. Furthermore, its original version had been coded to be compatible only with the free software OpenFOAM. The choice, subsequently made, to use SU2 as aerodynamic solver meant that an interface between the latter and PoliMIce was implemented and it had to be run separately from the PoliMIce main program, making the simulation loop more complicated. \\ 
As a consequence of these issues, in this thesis was developed a completely new version of PoliMIce which is compatible with both CFD solvers. The main innovation introduced into the new code is the characterization of three different zones in which are defined all the related problems: the body, the multiphase region and the flow. The zones are separeted by boundaries which allow the communication among the regions. Furthermore, any boundary can contain an arbitrary number of sub-boundaries allowing to deal a problem with multiple boundary conditions on the same geometric boundary. The ice accretion tools, already present in the previous version, were completely rewritten according to the new structure.
Furthermore, the water liquid film flowing above the ice surface in glaze conditions, was treated so far as isothermal and with constant thickness. In this thesis, different models for the water film were studied in detail and implemented in the new version of PoliMIce, in order to estimate its properties and its impact on an ice accretion simulation.   \\
Moreover, a mathematical model of an electro-thermal IPS was implemented. The reference design used for the IPS can be found in reference \cite{LSilva20071} and \cite{LSilva20072}. As water flows over the airfoil, a water film forms which exchanges heat with the surface and air. Therefore, the properties of the air thermal boundary layer cannot be extracted by the CFD solution as they change according on how the films flow on the surface. As a consequence of that, a model of the thermal and viscous bidimensional boundary layer was implemented and therefore their properties can be computed inside the solver at every iteration. \\
The \CC ~language was used to implement the data stuctures and the Unified Modeling Language (UML) was used to visualize the new design of the code and arrange all the classes, methods and attributes according to the wanted structure. The present work is organized as follows. \\
In chapter \ref{cap:introduction} was introduced the outline of the ice accretion problem in aviation, a brief description on the physics of ice was given, its most relevant parameters were discussed and the solutions which are nowadays available to protect aircraft components from ice formations were presented. \\
Chapter \ref{cap:icemodels} contains the ice accretion models based on the solution of the 1D Stefan problem. Both the quasi-steady and the unstady models are presented and all of them are implemented in PoliMIce. \\
Chapter \ref{cap:antiice} reports the design of the electro-thermal anti-ice system implemented in PoliMIce. Both the geometrical and thermodynamic aspects are presented including the model of the thermal and viscous boundary layer. \\
In Chapter \ref{cap:polimice} the new version of PoliMIce is presented. The code structure is described including all classes and their dependencies.  \\
Chapter \ref{cap:numsim} contains the results of the test cases used to validate the code. Both the ice accretion and the IPS are tested and compared to results obtained by experimental campaigns and other softwares. 




