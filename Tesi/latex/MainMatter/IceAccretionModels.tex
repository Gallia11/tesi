% ------------------------------------------------------------------------ 
% 	ICE ACCRETION MODELS
% ------------------------------------------------------------------------ 
%
\chapter{Ice accretion models}
%
\label{cap:icemodels}
%
% ------------------------------------------------------------------------ %
The study of the behaviour of a substance during its phase change was initially led by B.P. Clapeyron and G. Lamé in 1831 but the first mathematical description of the phenomenon was proposed by J.Stefan around 1890 during his studies in ice formation on the polar sea \cite{Stefan1891}. This research does not apply only to sea icing but it is valid for every multiphase substance in the freezing process. \\
The first relevant model of ice accretion used in aeronautics was proposed by Messinger in 1953 and its model is based on the energy balance taking into account all thermal fluxes between ice, water and the surrounding air. The assumptions of isothermal ice layer and insulated airfoil lead to important limitations of the model which result on an underestimation of the ice accretion respect to experiments. \\
More recently, the model proposed by Messinger has been revisited by Myers considering the conduction through the ice layer and removing the hypothesis of insulated surface \cite{Myers2001}. Myers model is based on an approximate solution of the Stefan problem and not on an a simple energy balance at the surface which leads to a formulation which better accounts for the two different mechanisms associated with rime and glaze ice formation. \\ 
In this chapter the most recent models based on a solution of the Stefan problem are presented. In addition to the aforementioned Myers model, a modified version of the latter improved by Gori \cite{Gori2015} and an unsteady accretion model developed by Parma \cite{Gori2018} are presented. All these models are implemented in PoliMice. 
%
% ------------------------------------------------------------------------ 
%
\section{The Stefan problem}
%
% ------------------------------------------------------------------------
%
The Stefan problem is a set of four Partial Differential Equations (PDEs) describing the evolution of a single-component two-phase substance during a phase change \cite{Gori2018}. Its solution gives the temperature distribution within the two-phase layers and the position of the two boundaries, one between the layers and the other between the substance and air. From the mathematical point of view, this problem belongs to the family of the \textit{moving boundary value problem} as the position of the interface between phases is unknown and depends on time and on the solution itself. Hence, the Stefan problem is a kind of \textit{free boundary problem} which it has to be solved for an unknown solution and an unknown boundary. \\
Considering the reference system in figure \ref{fig:refStefan}, the Stefan problem can be formulated as follows:
\begin{equation}
\left\{
\begin{array}{l}
\dfrac{\partial T}{\partial t} = \dfrac{K_i}{\rho_i C_{p_i}} \dfrac{\partial^2 T}{\partial z^2} \\
\dfrac{\partial \theta}{\partial t} = \dfrac{K_w}{\rho_w C_{p_w}} \dfrac{\partial^2 \theta}{\partial z^2} \\
\dot{m}_{fr} + \dot{m}_{h} = \dot{m}_{in} - \dot{m}_{out} \\
\Delta \dot{Q} = \dot{Q}_{top} + \dot{Q}_{bottom}
\end{array}
\right.
\label{eq:StefanProblem}
\end{equation}

\begin{figure}

	\centering
	\begin{tikzpicture}
		[axisline/.style={very thick,-stealth}]
		\draw [axisline] (0,0) -- (7,0) node[right]{$x$};
		
		\draw [axisline] (0,0) -- (0,6) node[right]{$z$};
		
  		\draw [cyan]     (0,3) -- (7,3) node[midway, below = 35pt] {ICE} node[pos = 1.08pt, black] {$B(t)$} node[pos = -0.1pt, below = 35pt, black] {$T(z,t)$};
  		
		\draw [cyan] (0,4.5) -- (7,4.5) node[midway, below = 15pt] {WATER} node[midway, above = 7pt, black] {AIR} node[pos = 1.17pt, black] {$B(t) + h(t)$} node[pos = -0.1pt, below = 15pt, black] {$\theta(z,t)$};
		
		\draw        (7,0)  -- (7,4.5);
		
		\draw [pattern = custom north east lines, hatchspread = 8pt, hatchthickness = 0.1pt, pattern color = gray, path picture = {\node[anchor=center]  at (path picture bounding box.center) {WALL};}] (0,0) rectangle (7, -1);
		
		\draw [heat] (1.5, 3) --++ (0, 0.8) node[pos = -0.5pt, below = 2pt, black] {$\dot{Q}_{bottom}$}; 
		
		\draw [heat] (1.5, 3) --++ (0, -0.8) node[pos = -0.8pt, above = 2pt, black] {$\dot{Q}_{top}$};
	\end{tikzpicture}

\caption{Reference system for an ice-water mixture over a surface}
\label{fig:refStefan}

\end{figure}

\noindent
The first two equations describe the heat diffusion within the ice and the water layer. The third equation is the mass conservation which states that the mass entering and leaving the system balances the mass of water which freezes increasing the ice thickness and the mass of water which remains liquid increasing the thickness of the water film. The last equation is called Stefan condition and it is an energy balance related to the heat fluxes involving the phase-change and thus it is evaluated at the interface. This equation states that the latent heat necessary to the phase-change balances the heat exchanged by the interface with the water and the ice layer. \\
The Stefan problem does not have a solution in closed form but, introducing some assumptions, the system of PDEs can be transformed in a system of ODEs which admits an analytical solution. This allows to understand the physics of the problem and gives the chance to implement accurate but simple models in ice accretion codes. \\
The ice accretion process is controlled by heat fluxes. Figure \ref{fig: HeatFluxes} shows all the thermal fluxes involved in the problem and a brief explanation of the main contributions in models is listed below:

\begin{figure}

	\centering
	\includegraphics[width=0.8\textwidth]{Images/IceAccretionModels/HeatFluxes.png}

	\caption{Heat fluxes involved in the ice accretion process}
	\label{fig: HeatFluxes}
\end{figure}

\begin{itemize}

	\item $\dot{Q}_c$ is the heat transferred by convection per unit time between air and ice or between air and water. It is defined as:
	\begin{equation}
		\dot{Q}_c = h_{c_{i/w}} (T_{i/w} - T_{air})A
	\end{equation}
	
	where $h_{c_{i/w}}$ is the heat transfer coefficient, ${i/w}$ stands for ice or water and $A$ is the area through which the heat transfer occours.
	
	\item $\dot{Q}_a$ is the thermal power due to the aerodynamic heating and it is caused by friction between air and the exposed surface:
	\begin{equation}
		\dot{Q}_a= \frac{1}{2} \frac{h_c R_c A V^2_{\infty}}{C_{p_{air}}}
	\end{equation}
	
	$h_c$ is the heat transfer coefficient between air and the surface, $R_c$ is the recovery factor which takes into the account the effects of air compressibility and $C_{p_{air}}$ is the air specific heat at constant pressure.
	
	\item $\dot{Q}_l$ is the latent heat per unit time released or absorbed during freezing or fusion:
	\begin{equation}
		\dot{Q}_l = \rho_i L_F \frac{\partial B}{\partial t} A
	\end{equation}
	
	where $\frac{\partial B}{\partial t}$ is the variation of ice thickness, $\rho_i$ the density of ice and $L_F$ the latent heat of fusion of ice. 
	
	\item $\dot{Q}_{e/s}$ is the heat transferred per unit time by evaporation of water or sublimation of ice. It is defined as:
	\begin{equation}
		\dot{Q}_{e/s} = \chi_{e/s}[e(T_{w/i}) - e(T_{air})]
	\end{equation}
	
	where $\chi_{e/s}$ is the evaporation or sublimation coefficient and $e(T)$ is called evaporation or sublimation function and returns the vapour pressure at a certain temperature.
	
	\item $\dot{Q}_{d}$ is the latent heat released per unit time by impinging droplets:
	\begin{equation}
		\dot{Q}_{d} = \beta \, LWC \, V_\infty \, C_{p_{air}} \, (T - T_d)
	\end{equation}
	
	where $T_d$ is the temperature of the droplet and $T$ the temperature of the surface.
	
	\item $\dot{Q}_{k}$ is the heat flux per unit time related to the kinetic energy of impinging droplets and it is defined as:
	\begin{equation}
		\dot{Q}_k = \frac{1}{2} \, \beta \, LWC \, A \, V^3_\infty
	\end{equation}
	
\end{itemize}
%
% ------------------------------------------------------------------------ 
%
\section{Myers model}
%
% ------------------------------------------------------------------------ 
%
The first ice accretion model presented in this thesis is the model developed by T. Myers in 2001. In its work, Myers proposed a simplified solution of the Stefan problem reported in \ref{eq:StefanProblem} introducing the effect of ice thermal conduction and the presence of liquid water. The model is based on several simplifications which allow to reduce the system of PDEs to a decoupled system of ODEs which admits a closed form solution. The main assumptions proposed by Myers are:

\begin{itemize}

	\item The properties of ice do not vary with temperature, thus the value of its density is fixed. However, it can take two different values whether rime or glaze ice forms.
	
	\item The temperature of the surface is fixed and known. This is justified by the great dimension of the body respect to the ice formation and its high thermal conductivity.
	
	\item Droplets are in thermal equilibrium with the surrounding air, which means that their temperatures are equal.
	
	\item The phase-change occours at a fixed temperature.
	
	\item The liquid film is considered infinitesimal, thus its height remains constant in time. This hypothesis is fairly well verified in aeronautical applications in icing conditions as the typical height of the water film is of the order of $\frac{1}{10}$ \si{\milli\metre} and allows to neglect its dynamics.
	
\end{itemize}

\noindent
With the aforementioned simplifications, the Stefan problem reported in \ref{eq:StefanProblem} becomes:
\begin{equation}
\left\{
\begin{array}{l}
\dfrac{\partial T}{\partial t} = \dfrac{K_i}{\rho_i C_{p_i}} \dfrac{\partial^2 T}{\partial z^2} \\
\dfrac{\partial \theta}{\partial t} = \dfrac{K_w}{\rho_w C_{p_w}} \dfrac{\partial^2 \theta}{\partial z^2} \\
\rho_i \dfrac{\partial B}{\partial t} = \beta \, LWC \, V_\infty \\
\rho_i L_F \dfrac{\partial B}{\partial t} = K_i \dfrac{\partial T}{\partial z} \Bigg|_{B(t)^{-}} - K_w \dfrac{\partial \theta}{\partial z} \Bigg|_{B(t)^{+}}
\end{array}
\right.
\label{eq:StefanProblemMyers}
\end{equation}

\noindent
Due to the hypothesis of infinitesimal thickness of the water layer, the only contribution to the accretion rate is due to the impinging droplets and this leads to the third equation. The energy balance is instead obtained substituting the heat Fourier's law on both sides of the interface. \\
Posing different boundary conditions to the system, two accretion laws can be obtained, one for the rime and one for the glaze ice. Furthermore, Myers introduced a parameter called \textit{rime limit thickness} which discerns whether rime or glaze ice occurs.

\subsubsection{Rime ice}
\noindent
In the case of rime ice, droplets immediatly freeze at the impact with the surface, thus no water film forms. The reference system which has to be considered is shown in figure \ref{fig:refRimeIce}.
 
\begin{figure}

	\centering
	\begin{tikzpicture}
		[axisline/.style={very thick,-stealth}]
		\draw [axisline] (0,0) -- (7,0) node[right]{$x$};
		
		\draw [axisline] (0,0) -- (0,5) node[right]{$z$};
		
  		\draw [cyan]     (0,3) -- (7,3) node[midway, below = 35pt] {ICE} node[pos = 1.08pt, black] {$B(t)$} node[pos = -0.1pt, below = 35pt, black] {$T(z,t)$};
  				
		\draw        (7,0)  -- (7,3);
		
		\draw [pattern = custom north east lines, hatchspread = 8pt, hatchthickness = 0.1pt, pattern color = gray, path picture = {\node[anchor=center]  at (path picture bounding box.center) {WALL};}] (0,0) rectangle (7, -1);
		
		\draw [heat] (1.5, 3) --++ (0, 0.8) node[pos = 2pt, below = 2pt, black] {$\dot{Q}_{top}$}; 
	\end{tikzpicture}

\caption{Reference system for a rime ice formation over a surface}
\label{fig:refRimeIce}

\end{figure}

\noindent
The rime ice thickness is immediately obtained integrating the mass balance which leads to an ice thickness growing linearly respect to time:
\begin{equation}
B(t) = \int_{0}^{t} \frac{\beta \, LWC \, V_\infty}{\rho_{ri}} d\tilde{t} =  \frac{\beta \, LWC \, V_\infty}{\rho_{ri}} t 
\label{eq:rimeThickness}
\end{equation}

\noindent
According to Myers' hypothesis, the surface is maintained at a constant, fixed and known temperature while at the ice-air interface a Neumann boundary condition is imposed stating that the heat flux on the ice surface is determined by convection, heat from incoming droplets, sublimation, aerodynamic heating, kinetic energy from incoming droplets and latent heat of freezing:
\begin{equation}
\left\{
\begin{array}{l}
T(0,t) = T_{wall} \\
-K_i \dfrac{\partial T}{\partial z} \Bigg|_B = \dfrac{\dot{Q}_c + \dot{Q}_d + \dot{Q}_s - \dot{Q}_a - \dot{Q}_k - \dot{Q}_l}{A} = \dfrac{\dot{Q}_{top}}{A}
\end{array}
\right.
\label{eq:RimeBC}
\end{equation}

\noindent
The heat diffusion within the ice layer is solved considering a quasi-steady approximation which allows to neglet the time derivative of the temperature, obtaining an ODE. The meaning of this simplification is that the ice growth rate is fairly slower than the heat conduction through the ice layer. However, this hypothesis can be applied only certain conditions and, according to Myers, only when the ice thickness is lower than approximately \SI{2.4}{\centi\metre} \cite{Myers2001}. Under this condition, the heat diffusion equation becomes:
\begin{equation}
\frac{\partial^2 T}{\partial z^2} \approx 0
\label{RimeQSODE}
\end{equation}

\noindent
The temperature profile can be obtained integrating twice the equation \ref{RimeQSODE} and applying the boundary conditions reported in \ref{eq:RimeBC}. This leads to a linear variation of the temperature within the ice layer:
\begin{equation}
T(z) = T_{wall} + \frac{\dot{Q}_a + \dot{Q}_k + \dot{Q}_l - \dot{Q}_c - \dot{Q}_d - \dot{Q}_s}{K_{i} A} z = T_{wall} - \frac{\dot{Q}_{top}}{K_i A} z
\label{TempRime}
\end{equation}

\noindent
Furthermore, Myers proposes a modification of the equation \ref{TempRime} which includes the effects of ice thickness when it is not negligible:
\begin{equation}
T(z) = T_{wall} + \frac{\dot{Q}_a + \dot{Q}_k + \dot{Q}_l - \dot{Q}_c - \dot{Q}_d - \dot{Q}_s}{A \left[K_i + \dfrac{B}{T_{wall} - T_{air}} \left(\dot{Q}_c + \dot{Q}_d + \dot{Q}_s \right)\right]} z
\label{TempRime}
\end{equation}

\subsubsection{Glaze ice}
\noindent
In the case of glaze ice, droplets do not freeze instantaneously at the impact point but they remain liquid forming a water film which flows over the clean and iced surface. In this situation, the reference system is the one reported in figure \ref{fig:refStefan}. Boundary conditions must be splitted in two parts because of the simultaneous presence of water and ice. The phase-change occours at the ice-water interface hence a Dirichelet condition setting the freezing temperature is there imposed. Instead, at the ice-air interface, a Neumann boundary condition sets heat fluxes on the surface:
\begin{align}
ICE: &\left\{
\begin{array}{l}
T(0,t) \; = T_{wall} \\
T(B,t) = T_{freezing}
\label{BCIceGlaze}
\end{array}
\right. \\
WATER: &\left\{
\begin{array}{l}
T(B,t) = T_{freezing} \\
-K_{w} \dfrac{\partial \theta}{\partial z} \Bigg|_{B+h} = \dfrac{\dot{Q}_c + \dot{Q}_e + \dot{Q}_d - \dot{Q}_a -\dot{Q}_k}{A}
\label{BCWaterGlaze}
\end{array}
\right.
\end{align}

\noindent
Using a similar argument to that in the rime ice, Myers introduced a quasi-steady approximation to solve the heat equations in both layers. For the approximation to remain valid, the liquid film must be very thin, approximately thinner than \SI{3}{\milli\metre} \cite{Myers2001}. However, in aeronautical applications, the liquid film thickness is about \SI{0.1}{\milli\metre} and thus this hypothesis is very effective. With this simplification, the heat equations become:
\begin{equation}
\frac{\partial^2 T}{\partial z^2} \approx 0
\label{eq:FourierApproxIce}
\smallskip
\end{equation}
\begin{equation}
\frac{\partial^2 \theta }{\partial z^2} \approx 0
\end{equation}

\noindent
Applying the boundary conditions \ref{BCIceGlaze} for ice and \ref{BCWaterGlaze} for water and integrating twice, a linear variation of the temperature in both layers is obtained:
\begin{equation}
T(z) = T_{wall} + \frac{T_{freezing} - T_{wall}}{B} z
\label{eq:TempMyersGlaze}
\smallskip
\end{equation}
\begin{equation}
\theta(z) = T_{freezing} + \frac{\dot{Q}_a + \dot{Q}_k - \dot{Q}_c - \dot{Q}_e - \dot{Q}_d}{A K_w} (z - B)
\end{equation}

\noindent
Even though the latter is formally linear with $z$, the temperature of the water film can be considered constant because of its small thickness:
\begin{equation}
\theta(z) \approx T_{freezing}
\end{equation}

\noindent
From now on, in this chapter, the water layer will be considerged isothermal and with constant height. However, this assumption will not be valid in the case of IPS in anti-ice condition and a model of the water film will be introduced in chapter \ref{cap:antiice}. \\
Due to the simultaneous presence of ice and water, the glaze ice accretion rate is obtained using the Stefan condition:
\begin{equation}
\begin{aligned}
\frac{\partial B}{\partial t} &= \frac{1}{\rho_{gi} L_F} \left( K_i \frac{\partial T}{\partial z} \Bigg|_{B(t)^{-}} - K_w \frac{\partial \theta}{\partial z} \Bigg|_{B(t)^{+}} \right) \\
&= \frac{1}{\rho_{gi} L_F} \left( K_i \frac{T_{freezing} - T_{wall}}{B} + \frac{\dot{Q}_c + \dot{Q}_e + \dot{Q}_d - \dot{Q}_a - \dot{Q}_k}{A} \right)
\end{aligned}
\label{GlazeAccrRate}
\end{equation}

\noindent
The equation \ref{GlazeAccrRate} implicitly depends on $B$ itself thus can be solved numerically discretizing in time the accretion problem.

\subsubsection{Rime limit thickness}
\noindent
For a smooth transition from rime to glaze, the ice and water thicknesses and growth rates must be continuous. Myers introduced the \textit{rime limit thickness}, which is the height at which glaze ice first appear and allows to discern whether rime or glaze models must be used. For ice thickness lower than the limit, ice follows the rime accretion law while for higher the phenomenon is described by the glaze model. To determine when this ocours, the ice growth rate from the mass balance is subsitued into the Stefan condition giving:
\begin{equation}
L_F \, \beta \, LWC \, V_\infty = K_i \frac{\partial T}{\partial z} \Bigg|_{B(t)^{-}} - K_w \frac{\partial \theta}{\partial z} \Bigg|_{B(t)^{+}}
\end{equation}

\noindent
The heat fluxes are set considering the corresponding solution of the heat equation evaluated at the interface when glaze ice first appear. Named $B_g$ the rime ice thickness, it yields:
\begin{equation}
L_F \, \beta \, LWC \, V_\infty = K_i \frac{T_{freezing} - T_{wall}}{Bg} + \frac{\dot{Q}_c + \dot{Q}_e + \dot{Q}_d - \dot{Q}_a - \dot{Q}_k}{A}  
\end{equation}

\noindent
Solving the last equation respect to $B_{g}$, the rime limit thickness results to be:
\begin{equation}
B_g = \frac{A K_i (T_{freezing} - T_{wall})}{A \, L_F \, \beta \, LWC \, V_\infty + \dot{Q}_a + \dot{Q}_k - \dot{Q}_c - \dot{Q}_e - \dot{Q}_d}
\label{eq:RimeLimitMyers}
\end{equation}

\noindent
The equation shows how the rime limit thickness depends on the environmental conditions, the collection efficiency and the freestream velocity. An important feature of this result is that it allows positive, negative and even infinite values for $B_g$ therefore it is possibile to discern three cases:
\begin{itemize}
	\item $B_g < 0$: glaze ice never appears
	\item $B_g > 0$ and $B < B_g$: rime ice conditions
	\item $B_g > 0$ and $B > B_g$: glaze ice conditions
\end{itemize}

\noindent
A negative value for $B_g$ could occur either because the numerator is lower than zero, that is, the surface is too warm for any
ice to grow. Alternatively, the denominator can be negative and this states there will never be enough energy in the system to produce water and the accretion is pure rime \cite{Myers2001}.
%
% ------------------------------------------------------------------------ 
%
\section{Modified Myers model}
%
% ------------------------------------------------------------------------ 
%
In 2013 Garabelli and Gori, in their master thesis defended at Politecnico di Milano, proposed an improved Myers model based on the same hypothesis but with some modifications regarding either the rime and the glaze ice \cite{GoriGarabelliMsc}. In the former case the mass conservation is modified considering a mass flux through neighbouring elements which allows to take into account the possibility that water can flow from a glaze mesh element to a neighbour rime element. In the latter, the temperature profile within the glaze ice is modified observing that the thermal conductivity of the wall is much higher than in the ice. These modifications lead to a new definition of the rime limit thickness which better agrees with experimental observations. Indeed, the Myers model provides an overestimation of the real ice shape.

\subsubsection{Rime ice}
\noindent
In the original Myers model, a surface rime element could collect water only due to the impinging droplets as reported in the mass balance in the Stefan problem \ref{eq:StefanProblemMyers}. In reality it can collect water also from neighbouring glaze elements which release water into it as shown in figure \ref{fig: 2DMassBalance}. 

\begin{figure}

	\centering
	\includegraphics[width=0.9\textwidth]{Images/IceAccretionModels/ModMyersMass.png}

	\caption{Mass balance on a surface element of a 2D accretion problem \cite{ParmaMsc}}
	\label{fig: 2DMassBalance}
	
\end{figure}

\noindent	
Recalling what reported in the general form of the Stefan problem \ref{eq:StefanProblem}, the mass conservation can be written as:
\begin{equation}
\dot{m}_{fr} + \dot{m}_h = \dot{m}_{in} - \dot{m}_{out}
\label{eq:massBalance}
\end{equation}

\noindent
The term $\dot{m}_h$ is neglected due to the hypothesis of constant and fixed thickness of the water layer. The term $\dot{m}_{in}$ is modified taking into account also the incoming water from neighbouring elements while $\dot{m}_{out}$ involves only the mass flux leaving the surface due to the sublimation of ice as from a rime element there is no outgoing water flux. With these considerations, the mass balance \ref{eq:massBalance} becomes:
\begin{equation}
A \rho_{ri} \frac{\partial B}{\partial t} = \dot{m}_{in} + A \, \rho_{ri} \, \beta \, LWC \, V_\infty - \frac{\dot{Q}_s}{L_S}
\label{eq:massBalanceSubs}
\end{equation}

\noindent
From the equation \ref{eq:massBalanceSubs}, the rime accretion rate can be computed dividing by $A \rho_{ri}$:
\begin{equation}
\frac{\partial B}{\partial t} = \frac{1}{\rho_{ri}} \left( \frac{\dot{m}_{in}}{A} + \beta \, LWC \, V_\infty - \frac{\dot{Q}_s}{A L_S} \right)
\label{eq:rimeRateMod}
\end{equation}

\noindent
The rime ice thickness is obtained integrating respect to time the equation \ref{eq:rimeRateMod} which leads again to a thickness growing linearly in time:
\begin{equation}
\begin{aligned}
B(t) &= \int_{0}^{t} \frac{1}{\rho_{ri}} \left( \frac{\dot{m}_{in}}{A} + \beta \, LWC \, V_\infty - \frac{\dot{Q}_s}{A L_S} \right) d \tilde{t} \\
&= \frac{1}{\rho_{ri}} \left( \frac{\dot{m}_{in}}{A} + \beta \, LWC \, V_\infty - \frac{\dot{Q}_s}{A L_S} \right) t
\end{aligned}
\label{eq:rimeThicknessMod}
\end{equation} 

\subsubsection{Glaze ice}
\noindent
The result of the Myers model in the case of glaze ice was a linear variation of the temperature within the ice layer, as shown by the equation \ref{eq:TempMyersGlaze}. The consequence of the hypothesis of high thermal conductivity of the wall is to have an infinite heat flux at the surface but this is not represented by a linear variation of the temperature within the ice as the heat flux is computed evaluating the derivative of the temperature profile according to the Fourier's law:
\begin{equation}
\frac{\partial T}{\partial z} \Bigg|_0 = \frac{\partial \left( T_{wall} + \dfrac{T_{freezing} - T_{wall}}{B} z \right)}{\partial z} \Bigg|_0 = \frac{T_{freezing} - T_{wall}}{B} 
\end{equation}

\noindent
Garabelli and Gori, in their master thesis \cite{GoriGarabelliMsc}, proposed a parabolic variation of the temperature within the ice layer in order to obtain an infinite heat flux at wall. Hence, the temperature varies as:
\begin{equation}
T(z) = a \sqrt{z} + b
\end{equation}

\noindent
Applying the boundary conditions \ref{BCIceGlaze}, the temperature profile within the glaze ice is:
\begin{equation}
T(z) = T_{wall} + \frac{(T_{freezing} - T_ {wall})}{\sqrt{B}} \sqrt{z}
\end{equation}

\noindent
Such as in the Myers model, the glaze ice accretion rate is obtained using the Stefan condition:
\begin{equation}
\begin{aligned}
\frac{\partial B}{\partial t} &= \frac{1}{\rho_{gi} L_F} \left( K_i \frac{\partial T}{\partial z} \Bigg|_{B(t)^{-}} - K_w \frac{\partial \theta}{\partial z} \Bigg|_{B(t)^{+}} \right) \\
&= \frac{1}{\rho_{gi} L_F} \left( K_i \frac{T_{freezing} - T_{wall}}{2B} + \frac{\dot{Q}_c + \dot{Q}_e + \dot{Q}_d - \dot{Q}_a - \dot{Q}_k}{A} \right)
\end{aligned}
\label{eq:GlazeRateMod}
\end{equation}

\noindent
It is important to notice that the first term in brackets is the half of the same term of the glaze accretion rate reported in the \ref{GlazeAccrRate}, computed by means of the Myers model. This leads to a slower accretion rate which better agrees with experimental results as the Myers model usually provides an overestimation of ice shapes. \\
However, the assumption of a parabolic temperature profile does not respect the heat diffusion equation in the quasi-steady form. Indeed, the result obtained integrating twice the equation \ref{eq:FourierApproxIce} is a temperature profile linearly depending on $z$. A part for this aspect, the improved Myers model provides better results in predicting ice shapes \cite{GoriGarabelliMsc}.

\subsubsection{Rime limit thickness}
\noindent
The rime limit thickess is computed likewise in the Myers model, substituting the ice growth rate into the Stefan condition: 
\begin{equation}
L_F \left( \frac{\dot{m}_{in}}{A} + \beta \, LWC \, V_\infty - \frac{\dot{Q}_s}{A L_S} \right) = K_i \frac{\partial T}{\partial z} \Bigg|_{B(t)^{-}} - K_w \frac{\partial \theta}{\partial z} \Bigg|_{B(t)^{+}}
\end{equation}

\noindent
The heat fluxes are evaluated at the interface when glaze ice first appear and that leads to:
\begin{equation}
L_F \left( \frac{\dot{m}_{in}}{A} + \beta \, LWC \, V_\infty - \frac{\dot{Q}_s}{A L_S} \right) = K_i \frac{T_{freezing} - T_{wall}}{2B_g} + \frac{\dot{Q}_{top}}{A}
\label{eq:RimelimitInterMod}
\end{equation}

\noindent
where $\dot{Q}_{top} = \dot{Q}_c + \dot{Q}_e + \dot{Q}_d - \dot{Q}_a - \dot{Q}_k$.

\noindent
Solving the equation \ref{eq:RimelimitInterMod} respect to $B_g$, the rime limit thickness results to be:
\begin{equation}
B_g = \frac{A K_i (T_{freezing} - T_{wall})}{2 \left[ L_F \left( \dot{m}_{in} + A \, \beta \, LWC \, V_\infty - \dfrac{\dot{Q}_s}{L_S} \right) \right] + \dot{Q}_a + \dot{Q}_k - \dot{Q}_c - \dot{Q}_e - \dot{Q}_d}
\label{eq:RimeLimitMod}
\end{equation}

\noindent
Also the rime limit thickness is smaller respect to the limit obtained in the Myers model. In this way the glaze ice condition is reached earlier and the final ice thickness at the end of the process is less than predicted by Myers. 
%
% ------------------------------------------------------------------------ 
%
\section{Unsteady accretion model}
%
% ------------------------------------------------------------------------ 
%
The ice accretion models previously considered are based on a quasi-steady approximation of the heat diffusion problem. The hypothesis which leads to this simplification is that the ice growth rate is slower than the heat conduction through the ice layer. Although this approximation allows to simplify the problem reducing a system of PDEs to a system of ODEs, in many conditions cannot be applied such as in very fast ice accretion. \\
In 2015 Parma, in his master thesis defended at Politecnico di Milano, proposed an exact unsteady accretion model in which the quasi-steady approximation is relaxed \cite{ParmaMsc}. The parabolic PDE governing the heat diffusion admits an exact similarity solution and this requires that boundary conditions satisfy certain properties. In particular it can be imposed a constant value of temperature or an heat flux proportional to $\sqrt{z}$ \cite{Lozano2007}. For this reason, the unsteady model developed by Parma holds only in the glaze ice accretion. In rime conditions, the Myers model and its modified version continue to be valid.

\subsubsection{Glaze ice}
\noindent
As already discussed, the heat conduction through the water layer can be neglected due to its small thickness. Therefore, the only heat diffusion problem considered is within the ice layer:
\begin{equation}
\frac{\partial T}{\partial t} = \alpha_i \frac{\partial ^2 T}{\partial z^2}
\label{eq:HeatDiffusion}
\end{equation}

\noindent
where $\alpha_i = \frac{K_i}{\rho_i C_{p_i}}$. The problem does not have a reference scale in space and in time, thus the temperature is expected to vary respect to a variable which is a combination of the independent variables. This means that a variation in space of the solution is also related to a variation in time. The solution of this particular problem can be found using the similarity approach, which means that the initial
problem is changed to a problem of only one variable made by the combination of other variables and the solution has everywhere the same shape but with a different scale factor \cite{ParmaMsc}. The similiarity variable can be defined as follows:
\begin{equation}
\xi(z,t) = \frac{z}{\sqrt{t}}
\end{equation}

\noindent
The equation \ref{eq:HeatDiffusion} can be expressed as a function of $\xi$, which is a function of a combination of $z$ and $t$. The variable $\Theta$, which represents the temperature depending on the similarity variable is introduced in order not to confuse the two solutions:
\begin{equation}
T(z,t) \longrightarrow \Theta(\xi(z,t))
\end{equation}

\noindent
The derivatives are rewritten respect to $\xi$:
\begin{equation}
\frac{\partial T}{\partial t} = \frac{\partial T}{\partial \xi} \frac{\partial \xi}{\partial t} = \Theta^{\prime} (\xi) \left( - \frac{1}{2} \frac{z}{\sqrt{t^3}} \right) = \Theta^{\prime} (\xi) \left( - \frac{1}{2} \frac{z}{t \sqrt{t}} \right) = - \frac{\xi}{2 t} \Theta^{\prime} (\xi)
\label{eq:timeDer}
\end{equation}
\begin{equation}
\frac{\partial^2 T}{\partial z^2} = \frac{\partial^2 T}{\partial \xi^2} \left( \frac{\partial \xi}{\partial z} \right)^2 + \frac{\partial T}{\partial \xi} \frac{\partial^2 \xi}{\partial z^2} = \frac{1}{t} \Theta^{\prime \prime} (\xi)
\label{eq:spaceDer}
\end{equation}

\noindent
Substituting the derivatives \ref{eq:timeDer} and \ref{eq:spaceDer} into the heat equation \ref{eq:HeatDiffusion}, the PDE reduces to a second order ODE where $\Theta$ is a function of the similarity variable:
\begin{equation}
\Theta^{\prime \prime} (\xi) + \frac{\xi}{2 \alpha_i} \Theta^{\prime} (\xi) = 0
\label{eq:odeSimilar}
\end{equation}

\noindent
Calling $\Theta^{\prime} (\xi) = F(\xi)$, the second order ODE reduces to first order ODE which solution is:
\begin{equation}
F(\xi) = A \exp \left( - \int_{0}^{\xi} \frac{s}{2 \alpha_i} ds \right) = A \exp \left( - \frac{\xi^2}{4 \alpha_i} \right)
\label{eq:fXi}
\end{equation}

\noindent
Integrating the equation \ref{eq:fXi} respect to $\xi$, the solution of the ODE \ref{eq:odeSimilar} is obtained:
\begin{equation}
\Theta(\xi) = A \int_{0}^{\xi} \exp \left( -\frac{s^2}{4 \alpha_i} \right) ds + D 
\end{equation}

\noindent
The solution depends on the Gaussian integral, therefore it can be written as a function of the error function as follows:
\begin{equation}
\Theta(\xi) = A \erf \left( \frac{\xi}{2 \sqrt{\alpha_i}} \right) + D
\end{equation}

\noindent
The integration constants $A$ and $D$ are determined using the boundary conditions \ref{BCIceGlaze} for the glaze ice problem. Introducing the parameter $\lambda = \frac{B}{2 \sqrt{\alpha_i t}}$ and replacing $\Theta(\xi(z,t))$ with $T(z, t)$, the similarity solution of the \ref{eq:HeatDiffusion} is finally obtained:
\begin{equation}
T(z,t) = T_{wall} + (T_{freezing} - T_{wall}) \frac{\erf \left( \dfrac{z}{2 \sqrt{\alpha_i t}} \right)}{\erf (\lambda)}
\end{equation}

\noindent
Figure \ref{fig:iceTemperatures} shows the temperature profiles within the glaze ice layer computed with the methods discussed so far. The unsteady profile is plotted at a fixed time. 

\begin{figure}

	\centering
	\begin{tikzpicture} 
    [declare function={erfpl(\x)=%
      (1+(e^(-(\x*\x))*(-265.057+abs(\x)*(-135.065+abs(\x)%
      *(-59.646+(-6.84727-0.777889*abs(\x))*abs(\x)))))%
      /(3.05259+abs(\x))^5)*(\x>0?1:-1);},]
      
		\begin{axis}[axis line style = very thick, every axis plot post/.append style= thick, xtick = {0.001,2}, xticklabels={$0$, $B$}, ytick = {10,20}, yticklabels={$T_{wall}$, $T_{freezing}$}, axis lines=center, xlabel={$z$}, ylabel={$T$}, xmin=0, xmax=2.5, ymin=8, ymax=22, legend style={at={(0.4,-0.1)},anchor=north}]  
        \addplot[samples=100, domain=0:2,] {10+5*x}; \addlegendentry{Myers model}
        \addplot[samples=100, domain=0:2, color=red] {10+(10/(2^0.5))*x^0.5}; \addlegendentry{Modified Myers model}
        \addplot[samples=100, domain=0:2, color=blue] {10 + 10*erfpl(x/2)/erfpl(1)}; \addlegendentry{Unsteady model}
        \draw [dashed, help lines] (axis cs:0,20) -| (axis cs:2,8);
        \end{axis}
	\end{tikzpicture}

\caption{Temperature profiles within the glaze ice layer}
\label{fig:iceTemperatures}
\end{figure}

\noindent
The ice accretion rate is computed by means of the Stefan condition, which is here recalled:
\begin{equation}
\rho_i L_F \frac{\partial B}{\partial t} = K_i \frac{\partial T}{\partial z} \Bigg|_{B(t)^{-}} - K_w \frac{\partial \theta}{\partial z} \Bigg|_{B(t)^{+}}
\end{equation}

\noindent
Using the notation previously introduced, the ice thickness can be expressed as a function of $\lambda$. Thus, the ice accretion rate becomes:
\begin{equation}
\frac{\partial B}{\partial t} = \frac{d}{dt} \left( 2 \sqrt{\alpha_i t} \lambda(t) \right) = 2 \sqrt{\alpha_i t} \frac{d\lambda(t)}{dt} + \lambda(t) \sqrt{\frac{\alpha_i}{t}}
\label{eq:dBdt}
\end{equation} 

\noindent
The heat fluxes are set considering the boundary conditions at the water-ice interface. The derivative of the exact temperature profile evaluated at the interface is:
\begin{equation}
\begin{aligned}
\frac{\partial T}{\partial z} \Bigg|_{B(t)^{-}} &= \frac{T_{freezing} - T_{wall}}{\erf(\lambda)} \frac{\partial}{\partial z} \left( \frac{2}{\sqrt{\pi}} \int_{0}^{\dfrac{z}{2 \sqrt{\alpha_i t}}} e^{-s^2} ds \right) \Bigg|_{B(t)^{-}} \\
&= \frac{T_{freezing} - T_{wall}}{\erf(\lambda)} \frac{2}{\sqrt{\pi}} \exp \left[- \left( \dfrac{B}{2 \sqrt{\alpha_i t}} \right)^2 \right] \frac{1}{2 \sqrt{\alpha_i t}} \\
&=  \frac{T_{freezing} - T_{wall}}{\erf(\lambda)} \frac{\exp(-\lambda^2)}{\sqrt{\pi \alpha_i t}}
\end{aligned} 
\label{eq:dTdz}
\end{equation}

\noindent
The heat flux on the water side of the interface is given by an energy balance, as reported in the boundary condition \ref{BCWaterGlaze}. The values of the heat flux at the interface \ref{eq:dTdz} and \ref{BCWaterGlaze}, together with the expression of the ice accretion rate \ref{eq:dBdt}, are replaced into the Stefan condition giving a non-linear ODE in the unknown $\lambda(t)$:
\begin{equation}
\frac{d \lambda(t)}{dt} + \frac{1}{2} \frac{\lambda(t)}{t} - \frac{K_i (T_{freezing} - T_{wall})}{2 \sqrt{\pi} \rho_i L_F \alpha_i} \frac{\exp (-\lambda(t)^2)}{t \erf(\lambda(t))} - \frac{\dot{Q}_{top}}{2A \rho_i L_F \sqrt{\alpha_i t}} = 0
\label{eq:lambdaFun}
\end{equation}

\noindent
The equation \ref{eq:lambdaFun} can be solved numerically discretizing the problem in time and providing an initial solution for $\lambda(t)$. Once $\lambda(t)$ is known, it is possibile to compute the ice thickness.

\subsubsection{Rime limit thickness}
\noindent
In order to discern whether rime or glaze ice accrete, the rime limit thickness is again computed imposing the water height to zero in the mass balance and substituting the ice accretion rate into the Stefan condition. Parma obtains a non-linear equation in $\lambda(t)$ which has to be solved iteratively. As this approach is quite slow from the computational point of view, the version of the rime limit thickness proposed by Garabelli and Gori reported in the previous section can be used as it is faster and more robust \cite{ParmaMsc}.